# Insert your preferred key mappings here.
map w scrollUp
map s scrollDown
map a scrollLeft
map d scrollRight
#设置上下左右方向键
map l scrollPageDown
map h scrollPageUp
#设置上下翻半页
map gg scrollToTop
map G scrollToBottom
#设置到顶部或底部
map j goBack
map k goForward
#设置前进后退

#f可以显示帮助
#L和K可以帮助跳转标签页
