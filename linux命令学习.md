ls

lsblk 列出设备信息

md5sum 计算和校验MD5信息签名

dd 转换和复制文件
history
adduser 添加用户
alias 创建别名
apg 创建任意复杂的随机密码
fdisk 创建分区
mkfs 创建文件系统
mount 挂载分区
mkswap 创建交换分区
aria2 下载工具
aria2c 快速下载工具（支持Http（s） FTP Sftp BT）
bc 一个计算器
bat 打印或者连接文件
bg 恢复已挂起的进程
blender
calc 终端上交互式任意精度计算器
chage 更改用户账户和密码的有效期限
chattr 修改文件或文件夹的属性
chroot 使用特殊根目录运行命令或交互shell
find 搜索文件
cmp 比较两个文件
grep 在指定文件中搜索指定字符串
ps
kill
whereis 定位命令的二进制文件、资源、帮助页
services 控制服务的启动、停止和重启
df 磁盘使用情况
lpr 将指定的文件在指定的打印机上打印
wget 非交互式下载工具 Http Https Ftp
gcc c语言的内置编译器 *.c文件
g++ c++的内置编译器 *.cpp文件
javac java编译器
code VS code编译器
column 将标准输入或文件内容格式化为分列显示形式
command 强制shell执行程序并忽略具有相同名称的任何参数，内置参数和别名
csc 微软的c#编译器
curl 上传或下载数据至服务器
deluser 删除用户
dpkg Debian包管理器
exa ls的现代替代品
expand 将制表符转换为空格
false 返回失败值 1
fd 一个简单快速和用户交互友好的’find‘替代品
fish 友好的互动外壳
ftp FTP服务的客户端交互工具
git
go
gpasswd 组密码
groupadd
groupmod
groupdel
groups 打印用户的组成员身份
head
hg
hostess 用于管理/etc/hosts 文件的工具
http-prompt 交互式HTTP客户端 自动补全+语法高亮
ip
julia
jupyter

