# VBA学习
## 变量

VBA变量名：中午 英文 数字 下划线

强制声明变量 Option Explicit 写在模块第一行

【dim （变量名）as （变量的类型）= 数据 】或者【 dim （变量名）= 数据】

【const （常量名）as （常量的类型）= 数据】

定义多个变量 dim = i ，p


```
用特殊字符代表数据类型，也可用与声明变量
数字后+%，设置数字为lnteger类型
数字后+&，设置数字为long类型
数字后+！，设置数字为single类型
数字后+#，设置数字为double类型
数字后+@，设置数字为currency类型
数字后+$，设置数字为string类型
```

## VBA内部函数
### 测试函数

```
IsNumeric(x)          ‘是否为数字,  返回Boolean结果，True  or False      
IsDate(x)              ‘是否是日期,  返回Boolean结果，True  or False
IsEmpty（x）         ‘是否为Empty, 返回Boolean结果，True  or False
IsArray(x)             ‘指出变量是否为一个数组。
IsError(expression)  ‘指出表达式是否为一个错误值
IsNull(expression)    ‘指出表达式是否不包含任何有效数据 (Null)。
IsObject(identifier)  ‘指出标识符是否表示对象变量
```
### 字符串函数

```
Trim(string)                       去掉string左右两端空白
Ltrim(string)                      去掉string左端空白
Rtrim(string)                      去掉string右端空白
Len(string)                        计算string长度
Left(string, x)                    取string左段x个字符组成的字符串
Right(string, x)                   取string右段x个字符组成的字符串
Mid(string, start,x)              取string从start位开始的x个字符组成的字符串
Ucase(string)                     转换为大写
Lcase(string)                      转换为小写
Space(x)                           返回x个空白的字符串
Asc(string)                         返回一个 integer，代表字符串中首字母的字符代码
Chr(charcode)                     返回 string,其中包含有与指定的字符代码相关的字符
LEN（字符串）返回字符串长度
Trim（字符串）去除字符串两边的空格
Replace（字符串，字符x,字符y）用字符y替换字符串中的字符x
UCase(字符串)将字符串全部大写
LCase(字符串)将字符串全部小写
Left（字符串，数字N）由左向右取除字符串中N个字符
Right（字符串，数字N）由右向左取除字符串中N个字符
Mid（字符串，数字N，数字M）从第N位开始，由左向右取除字符串中M个字符
inStr（字符串N，字符串M）从字符串N中由左向右寻找字符串M首次出现的位置，找不到则返回0
inStrReV（字符串N，字符串M）从字符串N中由右向左寻找字符串M首次出现的位置，找不到则返回0
Spilt（字符串，分列符号）（编号，从0开始）拆分字符串
DateSerial（年，月，日）构建日期
```

## 自定义函数

```
技巧： 函数的快速测试
你编写自定义函数后，你可以在立即窗口快速的测试它。打开立即窗口，输入一个问号（?）在函数名称前，可以显示该函数的计算结果。记住，要在括号里输入函数的参数值。 例如，输入：

? SumItUp(54, 367.24)

然后回车。你的函数使用参数m和n传递的数值进行计算，函数的结果显示在下一行：

421.24
```

## 字典

```
创建字典【set 字典 = CreateeObject("Scripting.Dictioary")】
字典添加内容【字典.Add "键","值"
                        字典("键名")="值"】
获取字典的值【字典("键名")】
判断关键字是否存在【字典.exists（"键名"）】
```

## 数组

```
定义数组【dim 数组名(上标数字 to 下标数字)
                 dim 数组名(下标数组)
                dim 数组名 = array(1,2,3,4,5)】
获取数组的值【数组名(1)】
数组赋值【数组(1) = 值】
```
## 循环语句
### For循环
```
For 变量 = 起始值 To 结束值 Step  步长值
      处理语句
Next  Words
```
### For Each循环

```
For Each 变量 In  集合
     处理语句
Next
```
## IF语句
### 单IF

```
If 条件Then
    语句1
    语句2
    语句N
End If
```

```
If 条件 Then

如果条件为真时要执行的语句

Else

如果条件为假时要执行的语句

End If
```

### 多IF

```
If 条件判断01  Then
     执行语句01
ElseIf 条件判断02  Then
     执行语句02
Else
     执行语句03
End If
```
### select case

```
Select Case 测试表达式
Case 表达式1
如果表达式1匹配测试表达式的语句
Case 表达式2
如果表达式2匹配测试表达式的语句
Case 表达式N
如果表达式N匹配测试表达式的语句
Case Else
如果没有表达式匹配测试表达式要执行的语句
End Select
```

## 其他


```
这里输入代码
```


```
VBA中单元格的表示【range("B2")】
条件判断：【if 条件1 Then
                       结果1
                    else if 条件2 Then
                       结果2        
                   else
                      结果3
                    end if  】
for循环【 for i = 1 to 10 step 1
                next】默认步长值为1
VBA中不要求强制缩进
英文单引号后可添加注释
Worksheets.Add 增加一张工作表
for语句中碰见 Exit for 会结束for循环
VBA中F8 逐句执行
While 循环    【do While 条件1
                                条件为真，持续执行，条件问价，推出循环
                         loop】
While 中添加计数器 i = i +1

cells（数字1，数字2）定义第几行几列的单元格       
VBA对象：application，Excel应用程序；workbook，工作簿； worksheet，工作表； range，单元格
引用工作表：【Worksheets(想选择第几张工作表，为数字).Select
                       Worksheets(想选择第几张工作表，为数字).Activate
                       Sheets（”工作表名“）.Select
                       Sheets（”工作表名“）.Activate】
Select可以选择多张工作表，但不可以选择隐藏工作表；Activate只可以选择一张工作表，但可以选择隐藏工作表
Sheets.Select 选中所有工作表
Activesheet 选择当前工作表
重命名：【Sheets（”工作表名“）.name=”名字“
                工作表名.name=”名字“】
新建工作表：【Sheets.Add
                        Sheets.Add before ：Sheets（”想在那种工作表之前添加“）
                        Sheets.Add after ：Sheets（”想在那种工作表之后添加“）
                        Sheets.Add before ：Sheets（”想在那种工作表之前添加“），count=3 想添加几张工作表】
                        Sheets.Add （before ：Sheets（”想在那种工作表之前添加“））.name=”名字“给新添加的工作表起名
sheets.delete 删除工作表
【Excle.Application.DisplayAlerts = True 】是否允许excel进行弹窗，True为允许，False为不允许
给对象赋值 【Set 变量名 = 赋值的内容】
定义函数【function 函数名（）
                End function 】

正则表达式【set 正则 = CreateObject("vbscript.regexp")
                    正则.Pattern = "正则表达式"
                    正则.Global = True】
结果集合.submatches

获取当前单元格的值【ActiveCell.Value】
Range的属性【End（参数）
                        EntireColumn
                        EntireRow
                        Fond
                        】


反斜线\:除法中只取整数部分

#表达式# 两个#之间中写入日期表达式，默认表达为月、日、年
```


## 造价内容


```
https://mp.weixin.qq.com/s/DoH34hX3QmfqxaMp9_svQA

临时块命令【B】

永久块命令【W】

https://www.xigua36.com/zongyi/yinianyiduxijudasai/1-2.html

定额计价模式
                    工料机法（人财机）
                    仿清单法
定额计价方法：省消耗量定额&费用定额&信息价
基价/定额单价
定额3中类型：1、只有消耗量；2、有消耗量，有定额价；3、消耗量、定额价、管理费、税金（仿清单计价）
费用定额（费用定额与消耗量定额一一对应）
计价解决问题：费用构成 +   费用计算 + 计算规定
消耗量定额：反馈人材机的用量指标
费用定额：造价费用构成解释与计算公式依据
建设工程施工现场消防安全技术规范
定额计价中 - 可计量费用：分部分项费用，措施费用
组价定额反馈人材机的用量及费用，管理费和税金

```

